const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    userName: String,
    accessToken: String,
    refreshToken: String,
    expiresAt: Date
});

const modelName = 'user';
const User = mongoose.model(modelName, userSchema);

module.exports = User;