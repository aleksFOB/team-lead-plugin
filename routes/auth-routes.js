const router = require('express').Router();
const passport = require('passport');
const User = require('../models/user-model');
const logUtils = require('../utils/log-utils');

router.get('/form', (req, res) => {
    res.render('form');
})

//auth with tempo
router.get('/tempo', passport.authenticate('oauth2', {}));

//TODO remove if not needed
//auth logout
router.get('/revoke', (req, res) => {
    //handle with passport
    const username = req.user.userName;
    User.remove({userName: req.user.userName}, function (err) {
        if (!err) {
            logUtils.logMessage(username + " removed successfuly");
            req.logout();
        } else {
            logUtils.logMessage(err);
        }
    });
    res.redirect('/auth/tempo');
});

//callback route for tempo to redirect to
router.get('/tempo/redirect', passport.authenticate('oauth2'), (req, res) => {
    res.render('form')
})

module.exports = router;