const router = require('express').Router();

const atlassianJson = require('../atlassian/atlassian-connect');
router.get('/1', (req, res) => {
    //handle with passport
    res.json(atlassianJson);
});

module.exports = router;