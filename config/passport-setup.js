const passport = require('passport');
const myStrategy = require('passport-oauth2');
const keys = require('./keys');
const sharedUser = require('../models/shared-user');
const User = require('../models/user-model');
const refresh = require('passport-oauth2-refresh');
const logUtils = require('../utils/log-utils');
const formatUtils = require('../utils/format-utils');

//serialize for cookie
passport.serializeUser((user, done) => {
    done(null, user.id);
});

//deserialize from cookie
passport.deserializeUser((id, done) => {
    User.findById(id).then((user) => {
        done(null, user);
    });
});

const strategyOptions = ({
    //options for strategy
    authorizationURL: 'https://fobsolutions.atlassian.net/plugins/servlet/ac/io.tempo.jira/oauth-authorize/',
    tokenURL: 'https://api.tempo.io/oauth/token/',
    clientID: keys.tempo.clientID,
    clientSecret: keys.tempo.clientSecret,
    callbackURL: "/auth/tempo/redirect"
});

passport.use(
    new myStrategy(strategyOptions, (accessToken, refreshToken, params, done) => {
        //check if user already in db
        User.findOne({
            userName: sharedUser.userName
        }).then((currentUser) => {
            if (currentUser) {
                //already have the user
                logUtils.logMessage('user is: ' + currentUser);
                done(null, currentUser);
            } else {
                //if not create user in db
                sharedUser.accessToken = accessToken;
                sharedUser.refreshToken = refreshToken;
                sharedUser.expiresAt = formatUtils.getExpirationDate();
                sharedUser.save().then((newUser) => {
                    logUtils.logMessage('new user created: ' + newUser)
                    done(null, newUser);
                });
            }
        });

    })
);
refresh.use(new myStrategy(strategyOptions, (accessToken, refreshToken, done) => {
    User.findOne({userName: sharedUser.userName}, function (err, user) {
        user.accessToken = accessToken;
        user.refreshToken = refreshToken;
        user.save();
    });
}));