const express = require('express');
const authRoutes = require('./routes/auth-routes');
const atlassianRoute = require('./routes/atlassian-routes');
const mongoose = require('mongoose');
const fs = require('fs');
const https = require('https');
const passportSetup = require('./config/passport-setup');
const keys = require('./config/keys');
const app = express();
const fetch = require('node-fetch');
const sharedUser = require('./models/shared-user');
const passport = require('passport');
const cookieSession = require('cookie-session');
const {catchAsync} = require('./utils/async-utils');
const formatUtils = require('./utils/format-utils');
const refresh = require('passport-oauth2-refresh');
const User = require('./models/user-model');
const logUtils = require('./utils/log-utils');

//set up https options TODO remove when in production
const options = {
    key: fs.readFileSync('hacksparrow-key.pem'),
    cert: fs.readFileSync('hacksparrow-cert.pem')
};

//set  up view engine
app.set('view engine', 'ejs');

app.use(cookieSession({
    maxAge: 30 * 24 * 60 * 60 * 1000,//30 days
    keys: [keys.session.cookieKey]
}));

//initialize passport
app.use(passport.initialize());
app.use(passport.session());

//connect to mongodb
mongoose.connect(keys.mongodb.dbURI, () => {
    logUtils.logMessage("Connected to db");
});

//setup routes
app.use('/auth', authRoutes);

//atlassian json
app.use('/atlassian', atlassianRoute);

// app.listen(3000, () => {
//     logUtils.logMessage('app now listening to reports on port 3000');
// });

var server = https.createServer(options, app);
server.listen(3001, function () {
    logUtils.logMessage("server running at https://localhost:3001/")
});

//create home route
app.get('/', (req, res) => {
    if (req.user) {
        //user has token
        res.redirect('/auth/form');
    } else {
        //user does not have token
        res.redirect('/auth/tempo');
    }
});

function refreshAccessToken(req, res) {
    refresh.requestNewAccessToken('oauth2', req.user.refreshToken, function (err, accessToken, refreshToken) {
        if (err || !accessToken) {
            logUtils.logMessage("Error refreshing access token: statusCode " + err.statusCode + ", statusText: " + err.statusText);
            User.findOneAndRemove({userName: req.user.userName}).then(function (err, response) {
                req.logout();
                res.redirect('/plugin');
            });
        } else {
            logUtils.logMessage("new ACCESSTOKEN: " + accessToken);
            const expirationDate = formatUtils.getExpirationDate();
            const newData = {accessToken: accessToken, expiresAt: expirationDate, refreshToken: refreshToken};
            User.findOneAndUpdate({userName: req.user.userName}, newData, function () {
                logUtils.logMessage("User's accessToken has been rewritten");
                req.user.accessToken = accessToken;
                req.user.refreshToken = refreshToken;
                req.user.expiresAt = expirationDate;
                res.redirect('/');
            });
        }
    })
}

app.get('/plugin', (req, res) => {
    const userName = req.query['user_id'];
    if (userName) {
        logUtils.logMessage("USERNAME IS: " + userName);
        sharedUser.userName = userName;
    }
    if (req.user && req.user.expiresAt < new Date()) {
        //token has expired
        refreshAccessToken(req, res);
    } else {
        res.redirect('/');
    }
});

app.get('/get/teams', ((req, res) => {
    fetch(`https://api.tempo.io/2/teams`,
        {
            method: 'GET',
            headers: {
                Authorization: "Bearer " + req.user.accessToken
            },
        }).then((teamsResponse) => {
        if (teamsResponse.status === 401) {
            res.status(teamsResponse.status).send({
                error: teamsResponse.statusText
            });
            return null;
        }
        const teamsJson = teamsResponse.json();
        return teamsJson;
    }).then((teamsJson) => {
        if (!teamsJson) {
            return;
        } else if (teamsJson.hasOwnProperty("errors")) {
            res.status("teamsJson.status").send({
                error: "teamsJson.errors[0].message"
            });
            return;
        }
        res.send(teamsJson);
    });
}));

app.get('/get/teamhours', catchAsync(async (req, res) => {
    const teamId = req.query.teamId;
    const months = req.query.months;
    const year = req.query.year;
    let teamTimes = {};
    for (let i = 0; i < months.length; i++) {
        const from = formatUtils.getFirstDateOfMonth(months[i], year);
        const to = formatUtils.getLastDateOfMonth(months[i], year);
        const teamTimesResp = await fetch(`https://api.tempo.io/2/timesheet-approvals/team/${teamId}?from=${from}&to=${to}`,
            {
                method: 'GET',
                headers: {
                    Authorization: "Bearer " + req.user.accessToken
                },
            });
        const teamTimesJson = await teamTimesResp.json();
        if (teamTimesJson.hasOwnProperty("errors")) {
            res.status(teamTimesResp.status).send({
                error: teamTimesJson.errors[0].message
            });
        }
        const results = teamTimesJson.results;
        formatUtils.addTeamTimes(teamTimes, results);
    }
    formatUtils.addSaldo(teamTimes);
    logUtils.logMessage(teamTimes);
    res.send(teamTimes);
}));