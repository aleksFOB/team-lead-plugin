module.exports = {
    addSaldo: function (teamHours) {
        for (let key in teamHours) {
            let requiredSeconds = teamHours[key].requiredSeconds;
            let timeSpentSeconds = teamHours[key].timeSpentSeconds;
            let saldo = timeSpentSeconds - requiredSeconds;
            teamHours[key]["saldo"] = saldo;
        }
    },
    getLastDateOfMonth: function (month, year) {
        var lastDay = new Date(year, month, 0);
        return formatDate(lastDay);
    },
    getFirstDateOfMonth: function (month, year) {
        var firstDay = new Date(year, month - 1, 1);
        return formatDate(firstDay);
    },
    addTeamTimes: function (teamHours, results) {
        for (let i = 0; i < results.length; i++) {
            const user = results[i].user;
            if (teamHours.hasOwnProperty(user.username)) {
                let userName = user.username;
                teamHours[userName].requiredSeconds = teamHours[userName].requiredSeconds + results[i].requiredSeconds;
                teamHours[userName].timeSpentSeconds = teamHours[userName].timeSpentSeconds + results[i].timeSpentSeconds;
            } else {
                let userName = user.username;
                teamHours[userName] = {
                    displayName: user.displayName,
                    requiredSeconds: results[i].requiredSeconds,
                    timeSpentSeconds: results[i].timeSpentSeconds
                }
            }
        }
    },
    getExpirationDate: function () {
        const today = new Date();
        let newDate = new Date();
        newDate.setDate(today.getDate() + 30);//hardcoded, tempo api returns 60 days
        return newDate;
    }
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}
