const keys = require('../config/keys');

module.exports = {
    logMessage: function (message) {
        if(keys.allowLogging){
            console.log(message);
        }
    }
}